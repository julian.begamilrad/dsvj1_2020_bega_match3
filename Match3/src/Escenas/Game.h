#ifndef GAME_H
#define GAME_H

#include <iostream>

#include "raylib.h"

#include "Global.h"
#include "Credits.h"

using namespace std;

const int gameRow = 9;
const int gameColumn = 9;
const int TIMELIMIT = 3;
const int POINTSTOWIN = 2500;

struct Item
{
	int type;
	Rectangle myBody;
	bool isSelected;
	Color myColor;
	bool isEmpty;
};
namespace ENDLESSJB
{
	class Game
	{
	public:

		Game();
		~Game();
		void gInit();
		void gInput();
		void gUpdate();
		void gDraw();
		
	private:
		void gameTime();
		void resetTypes();
		void checkSelections(int i, int j);
		void deSelect(int i, int j);
		bool checkNeighbors(int i, int j);
		void emptySlection();
		void setNewItemsPositions();
		void refillItems();

		Item gameItems[gameRow][gameColumn];
		int timer[TIMESECTION];

		int points;
		int actualSelection;
		float seconds;
		int selectedItems;
		int Jelly1Matches;
		int Jelly2Matches;
		int Jelly3Matches;
		int Jelly4Matches;
		
		bool musicOn;
		bool victory;
		bool gameEnded;
		bool inGame;
		bool pause;
		bool somethingIsSlected;		

		Vector2 mousePoint;
		Vector2 posPause;
		Vector2 SelectedTrail[81];
		Vector2 lastPosSelected;
		Vector2 penultimatePosSelected;

		Texture2D Jellys[4];
		Texture2D Background;
		Texture2D Board;
		Texture2D Selected;
		Texture2D LastSelected;
		Texture2D pauseTexture;
		Texture2D HUD;
		Texture2D Victory;
		Texture2D Defeat;
		Texture2D pauseButton;

		Music music;
		Sound clearSelection;

	};

}

#endif