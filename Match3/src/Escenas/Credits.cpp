#include "Credits.h"
namespace ENDLESSJB
{

	Credits::Credits()
	{
		
		CreditsTexture = LoadTexture("res/Textures/Credits.png");
		
	}
	Credits::~Credits()
	{
		UnloadTexture(CreditsTexture);
	}
	void Credits::cInput()
	{			
		if (IsKeyReleased(KEY_M))
		{			
			Global::setGamestatus(MENU);
		}
	}
	void Credits::cInit()
	{
		
	}
	void Credits::cDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;

		
			DrawTextureEx(CreditsTexture, pos, 0, scale, WHITE);

	}
	
}
