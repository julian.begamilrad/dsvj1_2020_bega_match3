#include "Game.h"
namespace ENDLESSJB
{
	Game::Game()
	{
		inGame = true;
		pause = false;
		seconds = STARTDEFAULTNUMBER;
		timer[SECONDS] = STARTDEFAULTNUMBER;
		timer[MINUTES] = STARTDEFAULTNUMBER;
		timer[HOURS] = STARTDEFAULTNUMBER;
		music = LoadMusicStream("res/GameMusic.mp3");
		clearSelection = LoadSound("res/ClearSlection.ogg");
		musicOn = true;
		mousePoint = { 0.0f, 0.0f };
		Background = LoadTexture("res/Textures/Background.png");
		Board = LoadTexture("res/Textures/Board.png");
		HUD = LoadTexture("res/Textures/HUD.png");
		Selected = LoadTexture("res/Textures/Selected.png");
		LastSelected = LoadTexture("res/Textures/LastSelect.png");
		Jellys[0] = LoadTexture("res/Textures/Jelly1.png");
		Jellys[1] = LoadTexture("res/Textures/Jelly2.png");
		Jellys[2] = LoadTexture("res/Textures/Jelly3.png");
		Jellys[3] = LoadTexture("res/Textures/Jelly4.png");
		pauseTexture = LoadTexture("res/Textures/Pause.png");
		Defeat = LoadTexture("res/Textures/Defeat.png");
		Victory = LoadTexture("res/Textures/Victory.png");
		pauseButton = LoadTexture("res/Textures/TextNormal.png");
		actualSelection = 0;
		posPause.x = 0;
		posPause.y = 0;
		somethingIsSlected = false;
		gameEnded = false;
		for (int i = 0; i < 81; i++)
		{
			SelectedTrail[i].x = -1;
			SelectedTrail[i].y = -1;
		}
		
		lastPosSelected.x = -1;
		lastPosSelected.y = -1;
		points = STARTDEFAULTNUMBER;
		selectedItems = 0;
		
		victory = false;
		for (int i = 0; i < gameRow; i++)
		{
			for (int j = 0; j < gameColumn; j++)
			{
				gameItems[j][i].isSelected = false;
				gameItems[j][i].type = 0;
				gameItems[j][i].myColor = WHITE;
				gameItems[j][i].myBody.height = GetScreenHeight() / (gameRow + 4);
				gameItems[j][i].myBody.width = gameItems[j][i].myBody.height;
				gameItems[j][i].myBody.x = gameItems[j][i].myBody.width + ((gameItems[j][i].myBody.width + (gameItems[j][i].myBody.width / gameColumn)) * j);
				gameItems[j][i].myBody.y = (gameItems[j][i].myBody.height * 2) + ((gameItems[j][i].myBody.height + (gameItems[j][i].myBody.height / gameRow)) * i);
				gameItems[j][i].isEmpty = false;
			}
		}
		Jelly1Matches = 0;
		Jelly2Matches = 0;
		Jelly3Matches = 0;
		Jelly4Matches = 0;
	}
	Game::~Game()
	{
		for (int i = 0; i < 4; i++)
		{
			UnloadTexture(Jellys[i]);
		}

		UnloadTexture(Background);
		UnloadTexture(Board);
		UnloadTexture(Selected);
		UnloadTexture(LastSelected);
		UnloadTexture(pauseTexture);
		UnloadTexture(HUD);
		UnloadTexture(Victory);
		UnloadTexture(Defeat);
		UnloadTexture(pauseButton);
		

		UnloadMusicStream(music);
		UnloadSound(clearSelection);
		

	}
	void Game::gInit()
	{
		inGame = true;
		pause = false;
		seconds = STARTDEFAULTNUMBER;
		timer[SECONDS] = STARTDEFAULTNUMBER;
		timer[MINUTES] = STARTDEFAULTNUMBER;
		timer[HOURS] = STARTDEFAULTNUMBER;
		points = STARTDEFAULTNUMBER;
		musicOn = true;
		PlayMusicStream(music);
		actualSelection = 0;
		somethingIsSlected = false;
		selectedItems = 0;
		victory = false;
		for (int i = 0; i < gameRow; i++)
		{
			for (int j = 0; j < gameColumn; j++)
			{
				gameItems[j][i].isSelected = false;
				gameItems[j][i].type = 0;
				gameItems[j][i].myColor = WHITE;
				gameItems[j][i].myBody.height = GetScreenHeight() / (gameRow + 4);
				gameItems[j][i].myBody.width = gameItems[j][i].myBody.height;
				gameItems[j][i].myBody.x = gameItems[j][i].myBody.width + ((gameItems[j][i].myBody.width + (gameItems[j][i].myBody.width / gameColumn))* j);
				gameItems[j][i].myBody.y = (gameItems[j][i].myBody.height * 2) + ((gameItems[j][i].myBody.height + (gameItems[j][i].myBody.height / gameRow))* i);
				gameItems[j][i].isEmpty = false;
			}
		}
		Jelly1Matches = 0;
		Jelly2Matches = 0;
		Jelly3Matches = 0;
		Jelly4Matches = 0;
		resetTypes();
		gameEnded = false;
		
	}
	void Game::gInput()
	{
		if (IsKeyReleased(KEY_P) && gameEnded == false)
		{
			pause = !pause;

			if (pause) PauseMusicStream(music);
			else if (musicOn) ResumeMusicStream(music);
		}
		if (IsKeyPressed(KEY_ENTER) && gameEnded == false)
		{
			emptySlection();
		}
		if (IsKeyPressed(KEY_ENTER) && gameEnded == true)
		{
			Global::setGamestatus(CREDITS);
		}
		if (IsKeyPressed(KEY_M) && gameEnded == true)
		{
			Global::setGamestatus(MENU);
			gInit();
		}
		if (IsKeyReleased(KEY_R) && gameEnded == true)
		{
			gInit();
		}
		
		if (IsKeyReleased(KEY_N))
		{
			if (musicOn)PauseMusicStream(music);
			else if (!pause)ResumeMusicStream(music);
			musicOn = !musicOn;
		}
		
		if (pause == true)
		{
			if (IsKeyReleased(KEY_M))
			{
				Global::setGamestatus(MENU);
				gInit();
				pause = false;
			}

			if (IsKeyReleased(KEY_R))
			{
				gInit();
				pause = false;
			}
			
		}
		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && gameEnded == false)
		{
			for (int i = 0; i < gameRow; i++)
			{
				for (int j = 0; j < gameColumn; j++)
				{
					if (CheckCollisionPointRec(mousePoint, gameItems[j][i].myBody))
					{
						checkSelections(i,j);
					}
				}
			}
		}
		if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON) && gameEnded == false)
		{
			for (int i = 0; i < gameRow; i++)
			{
				for (int j = 0; j < gameColumn; j++)
				{
					if (CheckCollisionPointRec(mousePoint, gameItems[j][i].myBody))
					{
						if (gameItems[j][i].isSelected == true)
						{
							deSelect(i, j);
						}
					}
				}
			}
		}	
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON) && gameEnded == false)
		{
			emptySlection();
			if (actualSelection == 2)
			{
				int a = lastPosSelected.y;
				int b = lastPosSelected.x;
				gameItems[a][b].isSelected = false;
			}
			if (actualSelection == 1)
			{
				int a = lastPosSelected.y;
				int b = lastPosSelected.x;
				gameItems[a][b].isSelected = false;
			}
			selectedItems = 0;
			penultimatePosSelected.x = -1;
			penultimatePosSelected.y = -1;
			lastPosSelected.x = -1;
			lastPosSelected.y = -1;
			for (int i = 0; i < gameRow; i++)
			{
				for (int j = 0; j < gameColumn; j++)
				{
					gameItems[j][i].isSelected = false;
				}
			}
		}

	}
	void Game::gUpdate()
	{
		mousePoint = GetMousePosition();
		UpdateMusicStream(music);
		if (pause != true)
		{
			if (timer[MINUTES] < TIMELIMIT)
			{				
				

				if (selectedItems == 0)
				{
					somethingIsSlected = false;
					actualSelection = 0;
				}
			}
			else
			{
				 gameEnded = true;
				if (points >= POINTSTOWIN + 1)
				{
					victory = true;
				}
			}
			gameTime();
		}
	}
	void Game::gDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;

		Vector2 BoardPos;
		BoardPos.x = gameItems[0][0].myBody.x - (gameItems[0][0].myBody.width / 2);
		BoardPos.y = gameItems[0][0].myBody.y - (gameItems[0][0].myBody.height / 2);



		DrawTextureEx(Background, pos, 0, scale, WHITE);
		DrawTextureEx(HUD, pos, 0, scale, WHITE);
		DrawTextureEx(Board, BoardPos, 0, scale*1.24, WHITE);
		
		for (int i = 0; i < gameRow; i++)
		{
			for (int j = 0; j < gameColumn; j++)
			{
				if (gameItems[j][i].isEmpty == false)
				{
					//DrawRectangle(gameItems[j][i].myBody.x, gameItems[j][i].myBody.y, gameItems[j][i].myBody.width, gameItems[j][i].myBody.height, gameItems[j][i].myColor);
				}
					
				Vector2 thisPos = { gameItems[j][i].myBody.x , gameItems[j][i].myBody.y };

			
				switch (gameItems[j][i].type)
				{
				case 0:

					break;
				case 1:
					DrawTextureEx(Jellys[0], thisPos, 0, scale * 0.27, WHITE);
					break;
				case 2:
					DrawTextureEx(Jellys[1], thisPos, 0, scale * 0.27, WHITE);
					break;
				case 3:
					DrawTextureEx(Jellys[2], thisPos, 0, scale * 0.27, WHITE);
					break;
				case 4:
					DrawTextureEx(Jellys[3], thisPos, 0, scale * 0.27, WHITE);
					break;

				default:
					break;
				}
				if (gameItems[j][i].isSelected == true)
				{
					if (lastPosSelected.x == i && lastPosSelected.y == j)
					{
						DrawTextureEx(LastSelected, thisPos, 0, scale * 0.27, WHITE);
					}
					/*else if(penultimatePosSelected.x == i && penultimatePosSelected.y == j)
					{
						DrawTextureEx(LastSelected, thisPos, 0, 0.14, WHITE);
					}*/
					else
					{
						DrawTextureEx(Selected, thisPos, 0, scale * 0.27, WHITE);
					}
					
				}
			}
		}
		DrawText(TextFormat(" %1i", Jelly1Matches), GetScreenWidth() - (GetScreenWidth() / THIRDDIVIDER) + (Global::TextSize / 2), GetScreenHeight() / 2.4 - Global::TextSize, Global::TextSize, WHITE);
		DrawText(TextFormat(" %1i", Jelly2Matches), GetScreenWidth() - (GetScreenWidth() / THIRDDIVIDER) + (Global::TextSize / 2), GetScreenHeight() / 1.9 - Global::TextSize, Global::TextSize, WHITE);
		DrawText(TextFormat(" %1i", Jelly3Matches), GetScreenWidth() - (GetScreenWidth() / THIRDDIVIDER) + (Global::TextSize / 2), GetScreenHeight() / 1.6 - Global::TextSize, Global::TextSize, WHITE);
		DrawText(TextFormat(" %1i", Jelly4Matches), GetScreenWidth() - (GetScreenWidth() / THIRDDIVIDER) + (Global::TextSize / 2), GetScreenHeight() / 1.37 - Global::TextSize, Global::TextSize, WHITE);
		
		if (timer[MINUTES] < TIMELIMIT) 
		{
		DrawText(TextFormat("TIME %1i", timer[MINUTES]), GetScreenWidth() / HALFDIVIDER - MeasureText("TIME 100 :", Global::TextSize) / HALFDIVIDER, GetScreenHeight() / 8 - Global::TextSize, Global::TextSize, GRAY);
		DrawText(TextFormat(": %1i", timer[SECONDS]), GetScreenWidth() / HALFDIVIDER + MeasureText("TIME 100 :", Global::TextSize) / 3, GetScreenHeight() / 8 - Global::TextSize, Global::TextSize, GRAY);
		}
		else
		{
			DrawText("TIME IS OUT", GetScreenWidth() / HALFDIVIDER - MeasureText("TIME IS OUT", Global::TextSize) / HALFDIVIDER, GetScreenHeight() / 8 - Global::TextSize, Global::TextSize, GRAY);
			if (victory == true)
			{
				DrawTextureEx(Victory, pos, 0, scale, WHITE);
			}
			else
			{
				DrawTextureEx(Defeat, pos, 0, scale, WHITE);
			}
		}
		
		DrawText(TextFormat("PTs %1i", points), GetScreenWidth() / 5.65 - MeasureText("PTs .....", Global::TextSize / HALFDIVIDER) , GetScreenHeight() / 7 - Global::TextSize, Global::TextSize/HALFDIVIDER, WHITE);
		if (pause == true)
		{
			DrawTextureEx(pauseTexture, posPause, 0, scale, WHITE);
		}
	}
	void Game::gameTime()
	{
		if (pause != true)
			seconds += GetFrameTime();
		if (seconds >= 1) {
			timer[SECONDS]++;
			if (timer[SECONDS] >= 60) {
				timer[SECONDS] = 0;
				timer[MINUTES]++;
			}
			if (timer[MINUTES] >= 60) {
				timer[MINUTES] = 0;
				timer[HOURS]++;
			}
			seconds = 0;
		}
	}

	void Game::resetTypes()
	{
		for (int i = 0; i < gameRow; i++)
		{
			for (int j = 0; j < gameColumn; j++)
			{
				gameItems[j][i].type = GetRandomValue(1,4);
				switch (gameItems[j][i].type)
				{
				case 0:
					gameItems[j][i].myColor = WHITE;
					break;
				case 1:
					gameItems[j][i].myColor = RED;
					break;
				case 2:
					gameItems[j][i].myColor = BLUE;
					break;
				case 3:
					gameItems[j][i].myColor = GREEN;
					break;
				case 4:
					gameItems[j][i].myColor = YELLOW;
					break;

				default:
					break;
				}
			}
		}
	}

	void Game::checkSelections(int i, int j)
	{
		if (actualSelection == 0)
		{
			actualSelection = gameItems[j][i].type;
			gameItems[j][i].isSelected = true;
			somethingIsSlected = true;
			gameItems[j][i].myColor = PINK;
			selectedItems = selectedItems + 1;

			for (int k = 0; k < 81; k++)
			{
				if (SelectedTrail[k].x == -1)
				{
					SelectedTrail[k].x = i;
					SelectedTrail[k].y = j;
					k = k + 81;
				}
				
			}
			lastPosSelected.x = i;
			lastPosSelected.y = j;
		}
		else
		{
			if (gameItems[j][i].type == actualSelection && gameItems[j][i].isSelected == false )
			{
				if (checkNeighbors(i, j))
				{
					gameItems[j][i].isSelected = true;
					gameItems[j][i].myColor = PINK;
					selectedItems = selectedItems + 1;

					for (int k = 0; k < 81; k++)
					{
						if (SelectedTrail[k].x == -1)
						{
							SelectedTrail[k].x = i;
							SelectedTrail[k].y = j;
							k = k + 81;
						}

					}
					penultimatePosSelected.x = lastPosSelected.x;
					penultimatePosSelected.y = lastPosSelected.y;
					lastPosSelected.x = i;
					lastPosSelected.y = j;
				}
			}
			else
			{
				int a = lastPosSelected.y;
				int b = lastPosSelected.x;
				if (penultimatePosSelected.x == i && penultimatePosSelected.y == j)
				{
					
					gameItems[a][b].isSelected = false;
					selectedItems = selectedItems - 1;
					switch (gameItems[j][i].type)
					{
					case 0:
						gameItems[j][i].myColor = WHITE;
						break;
					case 1:
						gameItems[j][i].myColor = RED;
						break;
					case 2:
						gameItems[j][i].myColor = BLUE;
						break;
					case 3:
						gameItems[j][i].myColor = GREEN;
						break;
					case 4:
						gameItems[j][i].myColor = YELLOW;
						break;

					default:
						break;
					}
					for (int k = 80; k >= 0; k--)
					{
						if (SelectedTrail[k].x == -1)
						{

						}
						else
						{
							if (k != 0)
							{
								if (k != 1)
								{
									penultimatePosSelected.x = SelectedTrail[k - 2].x;
									penultimatePosSelected.y = SelectedTrail[k - 2].y;
								}
								lastPosSelected.x = SelectedTrail[k - 1].x;
								lastPosSelected.y = SelectedTrail[k - 1].y;
								SelectedTrail[k].x = -1;
								SelectedTrail[k].y = -1;
							}
							else
							{
								lastPosSelected.x = -1;
								lastPosSelected.y = -1;
							}
							k = k - 81;
						}

					}

				}
			}
		}
	}

	void Game::deSelect(int i, int j)
	{
		if (lastPosSelected.x == i && lastPosSelected.y == j)
		{
			gameItems[j][i].isSelected = false;
			selectedItems = selectedItems - 1;
			switch (gameItems[j][i].type)
			{
			case 0:
				gameItems[j][i].myColor = WHITE;
				break;
			case 1:
				gameItems[j][i].myColor = RED;
				break;
			case 2:
				gameItems[j][i].myColor = BLUE;
				break;
			case 3:
				gameItems[j][i].myColor = GREEN;
				break;
			case 4:
				gameItems[j][i].myColor = YELLOW;
				break;

			default:
				break;
			}
			for (int k = 80; k >= 0; k--)
			{
				if (SelectedTrail[k].x == -1)
				{

				}
				else
				{
					if (k != 0)
					{
						if (k != 1)
						{
							penultimatePosSelected.x = SelectedTrail[k - 2].x;
							penultimatePosSelected.y = SelectedTrail[k - 2].y;
						}
						lastPosSelected.x = SelectedTrail[k - 1].x;
						lastPosSelected.y = SelectedTrail[k - 1].y;
						SelectedTrail[k].x = - 1;
						SelectedTrail[k].y = -1;
					}
					else
					{
						lastPosSelected.x = -1;
						lastPosSelected.y = -1;
					}
					k = k - 81;
				}

			}

		}

	}

	
	bool Game::checkNeighbors(int i, int j)
	{
		// Validacion de Esquina inferior derecha
		if (i == (gameRow - 1) && j == (gameColumn - 1))
		{
			if ( gameItems[j][i - 1].isSelected == true ||
				 gameItems[j - 1][i].isSelected == true ||
				gameItems[j - 1][i - 1].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Esquina superior izquierda
		if (i == (0) && j == (0))
		{
			if (gameItems[j][i + 1].isSelected == true ||
				gameItems[j + 1][i].isSelected == true ||
				gameItems[j + 1][i + 1].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Esquina superior derecha
		if (i == (0) && j == (gameColumn - 1))
		{
			if (gameItems[j][i + 1].isSelected == true ||
				gameItems[j - 1][i].isSelected == true ||
				gameItems[j - 1][i + 1].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Esquina Inferior izquierda
		if (i == (gameRow-1) && j == (0))
		{
			if (gameItems[j + 1][i].isSelected == true ||
				gameItems[j][i -1].isSelected == true ||
				gameItems[j + 1][i - 1].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Lateral Derecho
		if (i == (gameRow - 1) && j != (0) && j != (gameColumn-1))
		{
			if (gameItems[j][i - 1].isSelected == true || gameItems[j-1][i - 1].isSelected == true ||gameItems[j + 1][i - 1].isSelected == true
				|| gameItems[j + 1][i].isSelected == true || gameItems[j - 1][i].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Lateral Izquierdo
		if (i == (0) && j != (0) && j != (gameColumn - 1))
		{
			if (gameItems[j][i +1].isSelected == true || gameItems[j - 1][i+ 1].isSelected == true || gameItems[j + 1][i + 1].isSelected == true
				|| gameItems[j + 1][i].isSelected == true || gameItems[j - 1][i].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Ultima fila
		if (i != (gameRow - 1) && i != (0) && j == (gameColumn - 1))
		{
			if (gameItems[j][i - 1].isSelected == true || gameItems[j][i + 1].isSelected == true 
				|| gameItems[j - 1][i].isSelected == true || gameItems[j - 1][i +1].isSelected == true || gameItems[j - 1][i - 1].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		//Validacion de Primera fila
		if (i != (gameRow - 1) && i != (0) && j == 0)
		{
			if (gameItems[j][i - 1].isSelected == true || gameItems[j][i + 1].isSelected == true
				|| gameItems[j + 1][i].isSelected == true || gameItems[j + 1][i + 1].isSelected == true || gameItems[j + 1][i - 1].isSelected == true)
			{
				return true;
			}
			else
				return false;
		}
		// Validacion de no borde de mapa
		if (gameItems[j + 1][i].isSelected == true || gameItems[j + 1][i + 1].isSelected == true || gameItems[j + 1][i - 1].isSelected == true ||
			gameItems[j][i + 1].isSelected == true || gameItems[j][i - 1].isSelected == true ||
			gameItems[j - 1][i + 1].isSelected == true || gameItems[j - 1][i].isSelected == true || gameItems[j - 1][i - 1].isSelected == true)
		{
			return true;
		}
		else
			return false;
	}

	void Game::emptySlection()
	{

		if (selectedItems >= 3)
		{
			if (musicOn == true)
			{
				PlaySound(clearSelection);
			}
			points = points + selectedItems * 10;

			switch (actualSelection)
			{
			case 1:
				Jelly1Matches = Jelly1Matches + selectedItems;
				break;
			case 2:
				Jelly2Matches = Jelly2Matches + selectedItems;
				break;
			case 3:
				Jelly3Matches = Jelly3Matches + selectedItems;
				break;
			case 4:
				Jelly4Matches = Jelly4Matches + selectedItems;
				break;
			default:
				break;
			}
			
			
			for (int i = 0; i < gameRow; i++)
			{
				for (int j = 0; j < gameColumn; j++)
				{
					if (gameItems[j][i].isSelected == true)
					{
						gameItems[j][i].isEmpty = true;
					}
				}
			}
			setNewItemsPositions();
			somethingIsSlected = false;
			actualSelection = 0;
			for (int i = 0; i < gameRow; i++)
			{
				for (int j = 0; j < gameColumn; j++)
				{
					gameItems[j][i].isSelected = false;
				}
			}
			refillItems();
			selectedItems = 0;
			for (int i = 0; i < 81; i++)
			{
				SelectedTrail[i].x = -1;
				SelectedTrail[i].y = -1;
			}

			lastPosSelected.x = -1;
			lastPosSelected.y = -1;
		}
	}

	void Game::setNewItemsPositions()
	{
		for (int k = 0; k < gameRow; k++)
		{
			for (int i = 0; i < gameRow - 1; i++)
			{
				for (int j = 0; j < gameColumn; j++)
				{
					if (gameItems[j][i].isEmpty == false)
					{
						if (gameItems[j][i + 1].isEmpty == true)
						{
							gameItems[j][i + 1].isEmpty = false;
							gameItems[j][i + 1].type = gameItems[j][i].type;
							gameItems[j][i + 1].myColor = gameItems[j][i].myColor;
							gameItems[j][i].isEmpty = true;
						}
					}
				}
			}
		}
	}

	void Game::refillItems()
	{
		for (int i = 0; i < gameRow; i++)
		{
			for (int j = 0; j < gameColumn; j++)
			{
				if (gameItems[j][i].isEmpty == true)
				{
					gameItems[j][i].type = GetRandomValue(1, 4);
					switch (gameItems[j][i].type)
					{
					case 0:
						gameItems[j][i].myColor = WHITE;
						break;
					case 1:
						gameItems[j][i].myColor = RED;
						break;
					case 2:
						gameItems[j][i].myColor = BLUE;
						break;
					case 3:
						gameItems[j][i].myColor = GREEN;
						break;
					case 4:
						gameItems[j][i].myColor = YELLOW;
						break;

					default:
						break;
					}
					gameItems[j][i].isEmpty = false;
					gameItems[j][i].isSelected = false;
				}
			}
		}
	}
	

}
