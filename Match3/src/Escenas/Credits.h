#ifndef CREDITS_H
#define CREDITS_H

#include "Global.h"

#include "raylib.h"

using namespace std;
namespace ENDLESSJB
{
	class Credits
	{
	public:
		Credits();
		~Credits();
		void cInput();
		void cInit();
		void cDraw();

	private:
		Texture2D CreditsTexture;
	};

}
#endif