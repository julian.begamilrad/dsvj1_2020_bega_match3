#include "Menu.h"
namespace ENDLESSJB
{
	Menu::Menu()
	{
		actualOption = Play;
		Global::setGamestatus(MENU);
		isControlMenu = false;

		mousePoint.x = 0;
		mousePoint.y = 0;
		select = LoadSound("res/Select.ogg");
		moveSelection = LoadSound("res/MoveSelection.ogg");
		music = LoadMusicStream("res/MenuMusic.mp3");
		menuBase = LoadTexture("res/Textures/Background.png");
		controls = LoadTexture("res/Textures/Controls.png");

		resolution1 = LoadTexture("res/Textures/1024x576ResButton.png");
		resolution2 = LoadTexture("res/Textures/1366x768ResButton.png");
		resolution3 = LoadTexture("res/Textures/1600x900ResButton .png");
		buttons[0].x = GetScreenWidth()/5;
		buttons[0].y = GetScreenHeight() - GetScreenHeight() / 10;
		buttons[0].height = GetScreenHeight() / 12;
		buttons[0].width = GetScreenWidth() / 10;

		buttons[1].x = GetScreenWidth() /2;
		buttons[1].y = GetScreenHeight() - GetScreenHeight() / 10;
		buttons[1].height = GetScreenHeight() / 12;
		buttons[1].width = GetScreenWidth() / 10;

		buttons[2].x = GetScreenWidth() - GetScreenWidth() / 5;
		buttons[2].y = GetScreenHeight() - GetScreenHeight() / 10;
		buttons[2].height = GetScreenHeight() / 12;
		buttons[2].width = GetScreenWidth() / 10;

		musicOn = true;
	}
	Menu::~Menu()
	{
		UnloadSound(select);     
		UnloadSound(moveSelection);
		UnloadMusicStream(music);
		UnloadTexture(menuBase);
		UnloadTexture(controls);
		UnloadTexture(resolution1);
		UnloadTexture(resolution2);
		UnloadTexture(resolution3);

	}
	void Menu::mInit()
	{
		actualOption = Play;
		Global::setGamestatus(MENU);
		isControlMenu = false;
		if (musicOn)
		{
			PlayMusicStream(music);
		}
		
		buttons[0].x = GetScreenWidth() / 15;
		buttons[0].y = GetScreenHeight() - GetScreenHeight() / 5;
		buttons[0].height = GetScreenHeight() / 12;
		buttons[0].width = GetScreenWidth() / 10;

		buttons[1].x = GetScreenWidth() / 5;
		buttons[1].y = GetScreenHeight() - GetScreenHeight() / 5;
		buttons[1].height = GetScreenHeight() / 12;
		buttons[1].width = GetScreenWidth() / 10;

		buttons[2].x = GetScreenWidth() / 3;
		buttons[2].y = GetScreenHeight() - GetScreenHeight() / 5;
		buttons[2].height = GetScreenHeight() / 12;
		buttons[2].width = GetScreenWidth() / 10;

	}
	void Menu::mInput()
	{
		if (IsKeyReleased(KEY_N))
		{
			if (musicOn)PauseMusicStream(music);
			else ResumeMusicStream(music);
			musicOn = !musicOn;
		}
		if (IsKeyReleased(KEY_S) || IsKeyReleased(KEY_DOWN))
		{
			if (isControlMenu == false)
			{
				PlaySound(moveSelection);

				if (actualOption == Exit)
				{
					actualOption = Play;
				}
				else
				{
					actualOption++;
				}
			}
		}

		if (IsKeyReleased(KEY_W) || IsKeyReleased(KEY_UP))
		{
			if (isControlMenu == false)
			{
				PlaySound(moveSelection);
				if (actualOption == Play)
				{
					actualOption = Exit;
				}
				else
				{
					actualOption--;
				}
			}
		}
		if (IsKeyReleased(KEY_ENTER))
		{
			PlaySound(select);
			if (isControlMenu == false)
			{
				switch (actualOption)
				{
				case Play:
					Global::setGamestatus(GAME);
					break;
				case Controls:
					isControlMenu = true;
					break;
				case Credits:
					Global::setGamestatus(CREDITS);
					break;
				case Exit:
					Global::setGamestatus(EXIT);
					break;
				default:
					break;
				}

			}
			else
			{
				isControlMenu = false;
			}
		}
		if (IsKeyDown(KEY_ESCAPE))
		{
			Global::setGamestatus(EXIT);
		}

		if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
		{
			if (CheckCollisionPointRec(mousePoint, buttons[0]))
			{
				Global::ChangeSize1();
				mInit();
			}
			if (CheckCollisionPointRec(mousePoint, buttons[1]))
			{
				Global::ChangeSize2();
				mInit();
			}
			if (CheckCollisionPointRec(mousePoint, buttons[2]))
			{
				Global::ChangeSize3();
				mInit();
			}
		}
	}
	void Menu::mUpdate()
	{
		UpdateMusicStream(music);
		mousePoint = GetMousePosition();
	}
	void Menu::mDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;
		Vector2 posPlayer;
		posPlayer.x = GetScreenWidth()/3; posPlayer.y = GetScreenHeight()/2.5;
		float PJScale = scale / 1.3;

		
		
		if (isControlMenu == false)
		{
			DrawTextureEx(menuBase, pos, 0, scale , WHITE);
			
			DrawText("Play", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 2.6, Global::DISTANCEDEFAULT / 1.2, WHITE);
			DrawText("Controls", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 4.8, Global::DISTANCEDEFAULT / 1.8, WHITE);
			DrawText("Credits", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 6.8, Global::DISTANCEDEFAULT / 1.7, WHITE);
			DrawText("Exit", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 8.9, Global::DISTANCEDEFAULT / 1.2, WHITE);

			switch (actualOption)
			{
			case Play:
				DrawText("Play", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 2.6, Global::DISTANCEDEFAULT / 1.2, YELLOW);
				break;

			case Controls:
				DrawText("Controls", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 4.8, Global::DISTANCEDEFAULT / 1.8, YELLOW);
				break;

			case Credits:
				DrawText("Credits", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 6.8, Global::DISTANCEDEFAULT / 1.7, YELLOW);
				break;

			case Exit:
				DrawText("Exit", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 8.9, Global::DISTANCEDEFAULT / 1.2, YELLOW);
				break;
			default:
				break;
			}
			
			Vector2 posButton;
			posButton.x = buttons[0].x; posButton.y = buttons[0].y;
			DrawTextureEx(resolution1, posButton, 0, scale / 1.6, WHITE);

			posButton.x = buttons[1].x; posButton.y = buttons[1].y;
			DrawTextureEx(resolution2, posButton, 0, scale / 1.6, WHITE);

			posButton.x = buttons[2].x; posButton.y = buttons[2].y;
			DrawTextureEx(resolution3, posButton, 0, scale / 1.6, WHITE);

			/*DrawRectangle(buttons[0].x, buttons[0].y, buttons[0].width, buttons[0].height, RED);
			DrawRectangle(buttons[1].x, buttons[1].y, buttons[1].width, buttons[1].height, RED);
			DrawRectangle(buttons[2].x, buttons[2].y, buttons[2].width, buttons[2].height, RED);*/
		}
		if (isControlMenu == true)
		{
			DrawTextureEx(controls, pos, 0, scale, WHITE);
		}
	}
}
