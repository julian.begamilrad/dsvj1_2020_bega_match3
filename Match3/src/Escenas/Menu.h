#ifndef MENU_H
#define MENU_H

#include "raylib.h"

#include "Global.h"


namespace ENDLESSJB
{
	class Menu
	{
		enum MenuOptions
		{
			Play, Controls, Credits, Exit
		};

	public:
		Menu();
		~Menu();
		void mInit();
		void mInput();
		void mUpdate();
		void mDraw();

		//Textures menuTextures;
	private:

		int actualOption;
		bool isControlMenu;


		Vector2 mousePoint;
		Rectangle buttons[3];
		
		Sound select;
		Sound moveSelection;
		Music music;
		bool musicOn;
		Texture2D menuBase;
		Texture2D controls;

		Texture2D resolution1;
		Texture2D resolution2;
		Texture2D resolution3;
	};

}
#endif