#include "Global.h"
namespace ENDLESSJB
{
	float Global::TextSize = 0;
	float Global::DISTANCEDEFAULT = 0;
	float Global::PLAYERWIDTH = 0;
	float Global::PLAYERHEIGHT = 0;

	int Global::gamestatus = MENU;
	int Global::lastGamestatus = MENU;

	Global::Global()
	{
		gamestatus = MENU;
		lastGamestatus = MENU;
		TextSize = GetScreenHeight() / 12;
		DISTANCEDEFAULT = GetScreenHeight() / 15;

	}

	Global::~Global()
	{
		
		
	}
	int Global::getGamestatus()
	{
		return gamestatus;
	}
	int Global::getLastGamestatus()
	{
		return lastGamestatus;
	}
	void Global::setSize()
	{
		TextSize = GetScreenHeight() / 12;
		DISTANCEDEFAULT = GetScreenHeight() / 15;
		PLAYERWIDTH = GetScreenWidth() / 25;
		PLAYERHEIGHT = GetScreenHeight() / 8;
	}
	void Global::ChangeSize1()
	{
		SetWindowSize(1024, 576);
		setSize();
	}
	void Global::ChangeSize2()
	{
		SetWindowSize(1366, 768);
		setSize();

	}
	void Global::ChangeSize3()
	{
		SetWindowSize(1600, 900);
		setSize();
	}
	void Global::setGamestatus(int ngstatus)
	{
		gamestatus = ngstatus;
	}
	void Global::setLastGamestatus(int nlgstatus)
	{
		lastGamestatus = nlgstatus;
	}


}
