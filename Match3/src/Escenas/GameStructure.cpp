#include "GameStructure.h"
namespace ENDLESSJB
{
	GameStructure::GameStructure()
	{
		InitWindow(screenWidth, screenHeight, "Match 3 v1.0");
		InitAudioDevice();
		game = new Game();
		menu = new Menu();
		credits = new Credits();
		Global::setGamestatus(MENU); 
		Global::setLastGamestatus(MENU);
		firstTime = true;
		inGame = false;
		Global::setSize();
	}
	GameStructure::~GameStructure()
	{
		gsDeInit();
	}
	void GameStructure::gameLoop()
	{
		gsInit();
		while (inGame && !WindowShouldClose())
		{
			BeginDrawing();
			ClearBackground(DARKBROWN);
			gsInput();
			gsUpdate();
			gsDraw();
		}
	}
	void GameStructure::gsInit()
	{
		SetTargetFPS(60);
		menu->mInit();
		game->gInit();
		credits->cInit();
		Global::setGamestatus(MENU);
		Global::setLastGamestatus(MENU);
		inGame = true;
	}
	void GameStructure::gsInput()
	{
		switch (Global::getGamestatus())
		{
		case MENU:
			menu->mInput();
			break;
		case GAME:
			game->gInput();
			break;
		case CREDITS:
			credits->cInput();
			break;
		case EXIT:
			inGame = false;
			break;
		}
	
	}
	void GameStructure::gsUpdate()
	{
		if (Global::lastGamestatus != Global::gamestatus)
		{
			game->gInit();
		}
		switch (Global::getGamestatus())
		{
		case MENU:
			Global::setLastGamestatus(Global::getGamestatus());
			menu->mUpdate();
			break;
		case GAME:
			Global::setLastGamestatus(Global::getGamestatus());
			game->gUpdate();
			break;
		case CREDITS:
			Global::setLastGamestatus(Global::getGamestatus());
			break;	
		case EXIT:
			inGame = false;
			break;
		}
	}
	void GameStructure::gsDraw()
	{
		switch (Global::getGamestatus())
		{
		case MENU:
			menu->mDraw();
			break;
		case GAME:
			game->gDraw();
			break;
		case CREDITS:
			credits->cDraw();
			break;
		case EXIT:
			inGame = false;
			break;
		}
		EndDrawing();
	}
	void GameStructure::gsDeInit()
	{
		CloseAudioDevice();
		CloseWindow();
	}
}
